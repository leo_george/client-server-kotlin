package client

import java.io.DataOutputStream
import java.net.Socket

class Client (ipAddress:String,port: Int){
    private val socket: Socket = Socket(ipAddress,port)

    fun sendString(msg:String){
        val outputStream = DataOutputStream(socket.getOutputStream())
        outputStream.writeUTF(msg)
    }
}