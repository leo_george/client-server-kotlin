package server

import java.io.DataInputStream
import java.net.ServerSocket

class Server {
    companion object{
        private var INSTANCE:ServerSocket? = null
        private var isRunning = true

        fun getInstance(port:Int):Server{
            INSTANCE?: run {
                INSTANCE = ServerSocket(port)
            }
            return Server()
        }
    }
    fun runServer(){
        println("Server is running now")
        while (isRunning){
            val client = INSTANCE?.accept()
            val inputStream=DataInputStream(client?.getInputStream()!!)
            println(inputStream.readUTF())
        }
    }
    fun stopServer(){ isRunning =false }

}